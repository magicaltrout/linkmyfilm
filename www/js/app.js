// Dom7
var $$ = Dom7;

// Theme
var theme = 'auto';
if (document.location.search.indexOf('theme=') >= 0) {
    theme = document.location.search.split('theme=')[1].split('&')[0];
}
i = sinit()
mStartup(i)

var f7 = new Framework7({
    id: 'faboroto.linkmyfilm',
    root: '#app',
    pushState: true,
    theme: theme,
    routes: [
        {
            path: '/main/',
            url: './pages/main.html',
            name: 'home',
        },
        {
            path: '/',
            url: './splash.html',
            name: 'splash',

        },
        {
          path: '/menu',
          url: './pages/menu.html',
          name: 'menu'
        },
        {
          path: '/rules',
          url: './pages/rules.html',
          name: 'rules'
        }
    ],
    uid: i,
    data() {
      return {
        theme: this.theme
     }
   }
});

$$(document).on('page:init', '.page[data-name="menu"]', function (e) {
  state = loadState();
  if(state!=undefined && state.question_no<100){
    $('body').find('.resumelink').attr("href", "/main/?question_no="+state.question_no+"&score="+state.score)
    $('body').find('.resumebutton').show()

  }
  $(".facebookbutton").click(function () {
    ref = cordova.InAppBrowser.open('https://www.facebook.com/MovieLinkzApp/"', '_blank', 'location=yes');
    ref.show();

   });
$(".twitterbutton").click(function () {
    ref = cordova.InAppBrowser.open('https://twitter.com/movielinkzapp', '_blank', 'location=yes');
    ref.show();

   });

$(".facebookbutton").click(function () {
    ref = cordova.InAppBrowser.open('https://instagram.com/movielinkzapp', '_blank', 'location=yes');
    ref.show();

   });

$(".ratebutton").click(function(){
  AppRate.preferences.storeAppURL = {
  ios: '<my_app_id>',
  android: 'market://details?id=uk.faborto.linkmyfilm'
};

AppRate.preferences.customLocale = {
  title: "Would you mind rating %@?",
  message: "It will take more than a minute and helps to promote our app. Thanks for your support!",
  cancelButtonLabel: "No, Thanks",
  laterButtonLabel: "Remind Me Later",
  rateButtonLabel: "Rate It Now",
  yesButtonLabel: "Yes!",
  noButtonLabel: "Not really",
  appRatePromptTitle: 'Do you like using %@',
  feedbackPromptTitle: 'Mind giving us some feedback?',
};

AppRate.promptForRating();
})

})



$(document).on('deviceready', function () {
    var cssLabel = 0;


});
