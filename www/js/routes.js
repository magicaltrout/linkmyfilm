var routes = [
  // Index page
  {
    path: '/',
    url: './splash.html',
    name: 'home',
  },
  {
    path: '/splash',
    url: './splash.html',
    name: 'home',
  },
  {
    path:  '/main/',
    url: './pages/main.html',
    name: 'main'

  },
  // About page
  {
    path: '/about/',
    url: './pages/about.html',
    name: 'about',
  },

  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
