var correctMovieId;
var question_no = 1;
var score = 0;
var json;
var awards;
var scrollSnippet;
var posterSnippet;
var adIdx = 0;
var movingColumnHeight;
var scrollWidth;

$$(document).on('page:afterin', '.page[data-name="main"]', function (e,page) {

  if(page.route.query.score != undefined){
   score = Number(page.route.query.score);
   $("body").find(".curr_score").html(score);
  }
  else{
    score = 0
  }
  if(page.route.query.question_no != undefined){
    question_no = Number(page.route.query.question_no)+1
  }
  else{
    question_no = 1
  }
  init()
});

/**
 * Initialise the game display.
 */
init = function () {
  $.getJSON("gamepack/chain.json", function (j) {
    json = j;

    $.get("pages/scrollsnippet.html", function (html) {
      scrollSnippet = html;
      renderQ1(j);
    });

    $.get("pages/postersnippet.html", function (html) {
      posterSnippet = html;
    });

    $.getJSON("gamepack/awardstext.json", function (j) {
      awards = j;
    });
  });
  $("#option1").click(function () {
    if ($(this).data("ans") === true) {
      successfulans(this);
    }
    else {
      wrongans(this);
    }
  });
  $("#option2").click(function () {
    if ($(this).data("ans") === true) {
      successfulans(this);
    }
    else {
      wrongans(this);
    }
  });
  $("#option3").click(function () {
    if ($(this).data("ans") === true) {
      successfulans(this);
    }
    else {
      wrongans(this);
    }
  });

  $(".dismissaward").click(function () {
    $(".award-block").css("display", "none");
    $(".achievement-block").css("display", "block");
  });

  $(".dismissach").click(function () {
    $(".achievement-block").css("display", "none");
    if (question_no === 100) {
      $(".finished-block").css("display", "block");
    } else {
      $(".achievement").css("display", "none");
      moveon();
    }

  });

};

renderQ1 = function () {
  var c = $("#moving-column-holder");

  var jsonidx = question_no - 1;

  c.find("div:first").addClass("question" + question_no).append(scrollSnippet);
  c.find("div:first").attr("id", "moving-column" + question_no);

  movingColumnHeight = $(".moving-column-holder").height();
  scrollWidth = $(".question1").width();

  var height = movingColumnHeight;
  var width = scrollWidth;

  var middleColWidth = String(0.3 * height) + "px";
  var sideColWidth = String(0.5 * (width - (0.9 * height))) + "px";

  $(".question1").find(".col1").css("width", sideColWidth);
  $(".question1").find(".col2").css("width", middleColWidth);
  $(".question1").find(".col3").css("width", middleColWidth);
  $(".question1").find(".col4").css("width", middleColWidth);
  $(".question1").find(".col5").css("width", sideColWidth);

  c.find(".mvc:eq(1)").addClass("question" + (question_no + 1));
  c.find(".mvc:eq(1)").attr("id", "moving-column" + (question_no + 1));

  var q = json.Questions[jsonidx];

  var actor1 = q.Actor1;
  var actor2 = q.Actor2;

  // remove the connecting line from the first question only
  c.find(".question1").find(".prevline").css('border-bottom', 'none');

  // set actor 1
  c.find(".question" + question_no).find(".actor1").css('background-image', 'url(gamepack/actors/' + actor1._id + '.jpg)');
  c.find(".question" + question_no).find(".actor1name").text(actor1.Name);

  // set actor 2
  c.find(".question" + question_no).find(".actor2").css('background-image', 'url(gamepack/actors/' + actor2._id + '.jpg)');
  c.find(".question" + question_no).find(".actor2name").text(actor2.Name);

  var correct = q.Answers.Correct;
  var nearly = q.Answers.Nearly;
  var wrong = q.Answers.Wrong;

  correctMovieId = correct._id;

  // randomise answers
  var optArrays = [
    [3, 1, 2],
    [3, 2, 1],
    [1, 3, 2],
    [1, 2, 3],
    [2, 1, 3],
    [2, 3, 1]
  ]
  var num = Math.floor(Math.random() * 6);
  var optArray = optArrays[num];
  var x = optArray[0];
  var y = optArray[1];
  var z = optArray[2];

  $("#option" + x).text(correct.Name).data("ans", true);
  $("#option" + y).text(nearly.Name).data("ans", false);
  $("#option" + z).text(wrong.Name).data("ans", false);

  // OK - first Q loaded so prepare next
  renderNext((question_no) + 1);

}

renderNext = function (nextQ) {
  c = $("#moving-column-holder");

  c.find("#moving-column" + nextQ).append(scrollSnippet).addClass("question" + nextQ);;

  var height = movingColumnHeight;
  var width = scrollWidth;

  var middleColWidth = String(0.3 * height) + "px";
  var sideColWidth = String(0.5 * (width - (0.9 * height))) + "px";

  $(".question" + nextQ).find(".col1").css("width", sideColWidth);
  $(".question" + nextQ).find(".col2").css("width", middleColWidth);
  $(".question" + nextQ).find(".col3").css("width", middleColWidth);
  $(".question" + nextQ).find(".col4").css("width", middleColWidth);
  $(".question" + nextQ).find(".col5").css("width", sideColWidth);

  var jsonidx = nextQ - 1;
  var q = json.Questions[jsonidx];

  var actor1 = q.Actor1;
  var actor2 = q.Actor2;

  c.find(".question" + nextQ).find(".actor1").css('background-image', 'url(gamepack/actors/' + actor1._id + '.jpg)');
  c.find(".question" + nextQ).find(".actor1name").text(actor1.Name);

  c.find(".question" + nextQ).find(".actor2").css('background-image', 'url(gamepack/actors/' + actor2._id + '.jpg)');
  c.find(".question" + nextQ).find(".actor2name").text(actor2.Name);

  renderAdvert();
}

/**
 * Process an unsuccessful answer click.
 * @param el {object} the clicked object
 */
wrongans = function (el) {
    removeScore();
    $(el).addClass("wrongans");
    $(".question" + question_no).find(".questionmarkimage").css('background-image', 'url(img/wrong.png)');

    $(".question" + question_no).find(".purpleline").addClass("redline").removeClass("purpleline");
};

successfulans = function (el) {
  addScore();

  // make button green
  $(el).addClass("correctans");
  // make links green
  $(".question" + question_no).find(".purpleline").addClass("greenline").removeClass("purpleline");
  $(".question" + question_no).find(".redline").addClass("greenline").removeClass("redline");

  // disable all buttons
  ['#option1', '#option2', '#option3'].forEach(function(button) {
    $(button).addClass("disabled");
    $(button).attr("disabled", "true");
  });

  // add movie poster then move on / bring up awards screen
  $(".question" + question_no).find(".questionmarkrow").html(posterSnippet);
  $(".question" + question_no).find(".movieposter").css('background-image', 'url(gamepack/movies/' + correctMovieId + '.jpg)');

  for (var t = 0, i = 1; t <= (14*75), i <= 15; t += 75, i++) {
    doSetTimeout(i, t);
  }

  setTimeout(function () {
    $(".question" + question_no).find(".top-stars").attr('src', 'img/stars/0.png');;
  }, (15*75));
  setTimeout(function () {
    $(".question" + question_no).find(".bottom-stars").attr('src', 'img/starsups/0.png');;
  }, (15*75));

  setTimeout(function() {
    $(".question" + question_no).find("#row4A").css('background-image', 'none');
    $(".question" + question_no).find("#row4C").css('background-image', 'none');
  }, 500);

  if (Number.isInteger(question_no / 10)) {
    renderAwardScreen();
  } else {
    moveon();
  }
}

doSetTimeout = function(i, t) {
  setTimeout(function() {
    $(".question" + question_no).find(".top-stars").attr('src', 'img/stars/' + i + '.png');
  }, t);
  setTimeout(function() {
    $(".question" + question_no).find(".bottom-stars").attr('src', 'img/starsups/' + i + '.png');
  }, t);
}

moveon = function () {
  var waitTime = 1500;
  var scrollDuration = 750;
  var loadNextTime = waitTime + scrollDuration;

  // wait then scroll
  setTimeout(function () {
    $(".question" + (question_no + 1)).find(".linkdiv").addClass("left-border");
    $("#moving-column-holder").animate({"left": "-100%"}, {"duration": scrollDuration});
  }, waitTime);

  // what to do once scroll complete
  setTimeout(function () {
    // remove left border from previous link
    question_no = question_no + 1;
    $(".question" + question_no).find(".linkdiv").removeClass("left-border");

    // turn buttons purple again and enable them
    ['#option1', '#option2', '#option3'].forEach(function(button) {
      $(button).removeClass("wrongans");
      $(button).removeClass("correctans");
      $(button).removeClass("disabled");
      $(button).removeAttr("disabled");
    });

    // render text on answer buttons
    rendertext();

    // add empty div to the right
    var nextQ = question_no + 1;
    var prevQ = question_no - 1;
    $("#moving-column-holder").append('<div id="moving-column' + nextQ + '" class="col mvc question' + nextQ + '" style="height: 95%;"></div>')
    $("#moving-column-holder").find("#moving-column" + prevQ).remove();
    $("#moving-column-holder").stop().css("left", "0");

    renderNext(nextQ);

  }, loadNextTime);
};

rendertext = function () {
    var jsonidx = question_no - 1;
    var q = json.Questions[jsonidx];

    var correct = q.Answers.Correct;
    var nearly = q.Answers.Nearly;
    var wrong = q.Answers.Wrong;

    correctMovieId = correct._id;

    // randomise answers
    var optArrays = [
      [3, 1, 2],
      [3, 2, 1],
      [1, 3, 2],
      [1, 2, 3],
      [2, 1, 3],
      [2, 3, 1]
    ]
    var num = Math.floor(Math.random() * 6);
    var optArray = optArrays[num];
    var x = optArray[0];
    var y = optArray[1];
    var z = optArray[2];

    $("#option" + x).text(correct.Name).data("ans", true);
    $("#option" + y).text(nearly.Name).data("ans", false);
    $("#option" + z).text(wrong.Name).data("ans", false);
};


/**
 * Render the award popup on correct answer
 */
renderAwardScreen = function () {
    // function is run AFTER question_no is increased
    var a = $(".achievement");

    var q = String(question_no);
    a.find(".quote").text("\"" + awards[q].quote + "\"");
    a.find(".sub").text(awards[q].sub);
    a.find(".award").text(awards[q].award);
    a.find(".medal-img").css('background-image', 'url(gamepack/coins/' + q + '.png)');
    a.find(".ach-img").css('background-image', 'url(gamepack/achievements/' + q + '.png)');

    a.css("display", "block");
    a.find(".award-block").css("display", "block");

};

/**
 * Add to the score.
 */
addScore = function () {
    score = score + 1;
    var s = {"score":score, "question_no": question_no}
    saveState(s)
    $("body").find(".curr_score").html(score)
};

/**
 * Remove from the score.
 */
removeScore = function () {
    if (score > 5) {
        score = score - 5
    }
    else {
        score = 0
    }
    var s = {"score":score, "question_no": question_no}
    saveState(s)
    $("body").find(".curr_score").html(score)
};

renderAdvert = function () {
  $(".advert-div").css('background-image', 'url(img/ads/' + adIdx + '.png)');
  adIdx += 1;
};
