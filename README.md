# LinkMyFilm

## Installation

### Prerequisites

* Apache Cordova
* OpenJDK/Oracle JDK
* An IDE or Editor of choice
* Android SDK (for android testing)
* Mac OSX (for iOS testing)


### Checkout

    git clone git@gitlab.com:fabortoo/linkmyfilm.git

### Running

    cordova prepare
    cordova run browser -- --live-reload
or 

    cordova serve

## Code

Framework 7

- www/js/app.js Framework 7 app spec
- www/js/routes.js Framework 7 Routes
- www/js/index.js Cordova stuff

* index.html Main landing page
* pages/ Sub pages and fragments

## Release (Android)

cordova build --release android

mv apk to top directory

Get copy of keystore from Tom.

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore <apk>.apk MovieLinkz

zipalign -v 4 <apk>.apk <apk-signed>.apk

Upload to https://play.google.com/apps/publish/ and perform release

